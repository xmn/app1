package ru.xmn.app1.screens.person;

import ru.xmn.app1.mvp.presenter.BasePresenter;

/**
 * Created by xmn on 26.10.2016.
 */
public interface PersonPresenter extends BasePresenter<PersonView>{
    void loadPersonAndHouse(String personUrl, String houseUrl);
}
