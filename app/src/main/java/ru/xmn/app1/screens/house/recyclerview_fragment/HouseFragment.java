package ru.xmn.app1.screens.house.recyclerview_fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import ru.xmn.app1.R;
import ru.xmn.app1.mvp.model.data.Person;
import ru.xmn.app1.mvp.model.util.RealmString;
import ru.xmn.app1.screens.person.PersonActivity;
import rx.Observable;

/**
 * Created by xmn on 13.10.2016.
 */

public class HouseFragment extends Fragment implements HouseView {
    private String mHouseStr;
    private HouseAdapter mAdapter;
    private RecyclerView recyclerView;
    private RealmList<Person> mPersons = new RealmList<>();
//    private HousePresenter mPresenter = HousePresenterImpl.getInstance();
    private HousePresenter mPresenter = new HousePresenterImpl(this);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null && args.containsKey("HOUSE"))
            mHouseStr = args.getString("HOUSE");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_house,null);
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view_house);

        mAdapter = new HouseAdapter(mPersons, mHouseStr);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Person person = mPersons.get(position);
                Bundle bundle = new Bundle();
                bundle.putString("PERSON", person.getUrl());
                bundle.putString("HOUSE", mHouseStr);
                Intent i = new Intent(getActivity(), PersonActivity.class);
                i.putExtras(bundle);
                getActivity().startActivity(i);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
mPresenter.takeView(this);

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.initView();
        mPresenter.loadPersonList(mHouseStr);
    }


    @Override
    public void onPersonsLoaded(List<Person> persons) {
        mPersons.addAll(persons);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showError(Throwable error) {

    }

    @Override
    public void showLoad() {

    }

    @Override
    public void hideLoad() {

    }

    @Override
    public HousePresenter getPresenter() {
        return mPresenter;
    }
}
