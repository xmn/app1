package ru.xmn.app1.screens.house.recyclerview_fragment;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import io.realm.RealmList;
import ru.xmn.app1.Application;
import ru.xmn.app1.R;
import ru.xmn.app1.mvp.model.data.Person;

/**
 * Created by xmn on 15.10.2016.
 */

public class HouseAdapter extends RecyclerView.Adapter<HouseAdapter.MyViewHolder> {
    RealmList<Person> mPersons;
    private String mHouseStr;
    private int icon;

    public HouseAdapter(RealmList<Person> persons, String houseStr) {
        mPersons = persons;
        mHouseStr = houseStr;

        switch (mHouseStr.replaceAll("\\D+", "")) {
            case "362":
                icon = R.drawable.stark_icon;
                break;
            case "229":
                icon = R.drawable.lanister_icon;
                break;
            case "378":
                icon = R.drawable.targarien_icon;
                break;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView icon;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            icon = (ImageView) view.findViewById(R.id.house_icon);
        }
    }

    public HouseAdapter(RealmList<Person> persons) {
        mPersons = persons;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_holder_house, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Person person = mPersons.get(position);
        holder.name.setText(person.getName());
        Picasso.with(Application.get())
                .load(icon)
                .into(holder.icon);
    }

    @Override
    public int getItemCount() {
        return mPersons.size();
    }
}
