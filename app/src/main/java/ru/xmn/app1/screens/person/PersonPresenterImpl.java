package ru.xmn.app1.screens.person;

import android.support.annotation.Nullable;

import io.realm.Realm;
import ru.xmn.app1.mvp.model.RealmProvider;
import rx.Observable;

/**
 * Created by xmn on 26.10.2016.
 */

public class PersonPresenterImpl implements PersonPresenter {
    private final RealmProvider mRealmProvider;
    private PersonView mView;
    private static PersonPresenter sInstance = new PersonPresenterImpl();

    private PersonPresenterImpl() {
        mRealmProvider = new RealmProvider();
    }

    public static PersonPresenter getInstance() {
        return sInstance;
    }

    @Override
    public void takeView(PersonView view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public void initView() {
    }

    @Nullable
    @Override
    public PersonView getView() {
        return mView;
    }

    @Override
    public void loadPersonAndHouse(String personUrl, String houseUrl){
        Realm realm = Realm.getDefaultInstance();
        Observable.zip(mRealmProvider.getHouse(realm, houseUrl),
                mRealmProvider.getPerson(realm, personUrl), (house, person) ->
                {mView.onPersonAndHouseLoaded(person, house);
                return Observable.just(true);})
                .subscribe(booleanObservable -> realm.close());
    }
}
