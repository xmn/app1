package ru.xmn.app1.screens.splash;

import ru.xmn.app1.mvp.presenter.BasePresenter;
import ru.xmn.app1.mvp.view.BaseView;

/**
 * Created by xmn on 24.10.2016.
 */
public interface SplashPresenter extends BasePresenter<SplashView>{
}
