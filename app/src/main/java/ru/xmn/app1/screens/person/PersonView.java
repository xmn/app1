package ru.xmn.app1.screens.person;

import ru.xmn.app1.mvp.model.data.House;
import ru.xmn.app1.mvp.model.data.Person;
import ru.xmn.app1.mvp.presenter.BasePresenter;
import ru.xmn.app1.mvp.view.BaseView;

/**
 * Created by xmn on 26.10.2016.
 */

public interface PersonView extends BaseView<PersonPresenter> {
    void onPersonAndHouseLoaded(Person person, House house);
}
