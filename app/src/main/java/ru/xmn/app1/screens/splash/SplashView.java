package ru.xmn.app1.screens.splash;

import ru.xmn.app1.mvp.view.BaseView;

/**
 * Created by xmn on 13.10.2016.
 */
public interface SplashView extends BaseView<SplashPresenter> {
    public void loadCompleted();
}
