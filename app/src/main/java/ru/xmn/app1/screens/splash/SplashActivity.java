package ru.xmn.app1.screens.splash;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.view.MenuItem;

import ru.xmn.app1.mvp.view.SingleFragmentActivity;

public class SplashActivity extends SingleFragmentActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected Fragment createFragment() {
        return new SplashFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }
}
