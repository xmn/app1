package ru.xmn.app1.screens.house;

import ru.xmn.app1.mvp.presenter.BasePresenter;

/**
 * Created by xmn on 25.10.2016.
 */
public interface HouseTabPresenter extends BasePresenter<HouseTabView>{
}
