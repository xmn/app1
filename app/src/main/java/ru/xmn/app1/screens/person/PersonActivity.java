package ru.xmn.app1.screens.person;

import android.support.v4.app.Fragment;

import ru.xmn.app1.mvp.view.SingleFragmentActivity;

public class PersonActivity extends SingleFragmentActivity
         {

             @Override
             protected Fragment createFragment() {
                 return new PersonFragment();
             }
         }
