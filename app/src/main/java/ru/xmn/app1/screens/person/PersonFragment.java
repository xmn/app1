package ru.xmn.app1.screens.person;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import ru.xmn.app1.Application;
import ru.xmn.app1.R;
import ru.xmn.app1.mvp.model.data.House;
import ru.xmn.app1.mvp.model.data.Person;
import ru.xmn.app1.mvp.model.util.RealmString;

/**
 * Created by xmn on 13.10.2016.
 */

public class PersonFragment extends Fragment implements PersonView {
    private Person mPerson;
    private TextView wordsTv;
    private TextView bornTv;
    private TextView titlesTv;
    private TextView aliasesTv;
    private Button fatherBtn;
    private Button motherBtn;
    private House mHouse;
    private int icon;
    private ImageView image;
    private CollapsingToolbarLayout toolbar;
    PersonPresenter mPresenter = PersonPresenterImpl.getInstance();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_person, null);
        mPresenter.takeView(this);
        mPresenter.initView();
        findPerson();
        wordsTv = (TextView) v.findViewById(R.id.words_label);
        bornTv = (TextView) v.findViewById(R.id.born_label);
        titlesTv = (TextView) v.findViewById(R.id.titles_label);
        aliasesTv = (TextView) v.findViewById(R.id.aliases_label);
        fatherBtn = (Button) v.findViewById(R.id.father_button);
        motherBtn = (Button) v.findViewById(R.id.mother_button);
        image = (ImageView) v.findViewById(R.id.image);

        wordsTv.setText(mHouse.getWords().equals("") ? "n/a" : mHouse.getWords());
        bornTv.setText(mPerson.getBorn().equals("") ? "n/a" : mPerson.getBorn());
        titlesTv.setText(rlmLstToStr(mPerson.getTitles()));
        aliasesTv.setText(rlmLstToStr(mPerson.getAliases()));

        if (!mPerson.getFather().equals("")) {
            fatherBtn.setText(mPerson.getFather());
        } else {
            fatherBtn.setVisibility(View.GONE);
        }
        if (!mPerson.getMother().equals("")) {
            motherBtn.setText(mPerson.getMother());
        } else {
            motherBtn.setVisibility(View.GONE);
        }

        switch (mHouse.getUrl().replaceAll("\\D+", "")) {
            case "362":
                icon = R.drawable.stark;
                break;
            case "229":
                icon = R.drawable.lannister;
                break;
            case "378":
                icon = R.drawable.targarien;
                break;
        }

        Picasso.with(Application.get())
                .load(icon)
                .into(image);

        toolbar = (CollapsingToolbarLayout) v.findViewById(R.id.collapsing_toolbar);
        toolbar.setTitle(mPerson.getName());
        return v;
    }

    private String rlmLstToStr(RealmList<RealmString> list) {
        if (list.size() > 0) {
            String result = "";
            for (RealmString str :
                    list) {
                result += str.value + "\n";
            }
            if (result.contains("\n"))
                result = result.substring(0, result.lastIndexOf("\n"));
            return result;
        } else {
            return "n/a";
        }
    }

    private void findPerson() {
        Bundle args = getArguments();
        String persStr;
        String houseStr;
        persStr = args.getString("PERSON");
        houseStr = args.getString("HOUSE");

        Realm realm = Realm.getDefaultInstance();
        RealmResults<Person> persons = realm.where(Person.class).equalTo("url", persStr).findAll();
        RealmList<Person> results = new RealmList<>();
        results.addAll(persons.subList(0, persons.size()));
        mPerson = results.get(0);

        RealmResults<House> houses = realm.where(House.class).equalTo("url", houseStr).findAll();
        RealmList<House> resultsHouses = new RealmList<>();
        resultsHouses.addAll(houses.subList(0, houses.size()));
        mHouse = resultsHouses.get(0);
    }

    @Override
    public void onPersonAndHouseLoaded(Person person, House house) {
        mPerson=person;
        mHouse=house;
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showError(Throwable error) {

    }

    @Override
    public void showLoad() {

    }

    @Override
    public void hideLoad() {

    }

    @Override
    public PersonPresenter getPresenter() {
        return null;
    }
}
