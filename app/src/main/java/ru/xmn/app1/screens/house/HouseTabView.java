package ru.xmn.app1.screens.house;

import java.util.List;

import ru.xmn.app1.mvp.model.data.House;
import ru.xmn.app1.mvp.view.BaseView;

/**
 * Created by xmn on 25.10.2016.
 */

public interface HouseTabView extends BaseView<HouseTabPresenter> {
    void onHousesLoaded(List<House> houses);
}
