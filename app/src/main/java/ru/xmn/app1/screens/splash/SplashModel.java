package ru.xmn.app1.screens.splash;

import android.util.Log;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.xmn.app1.mvp.model.RealmProvider;
import ru.xmn.app1.mvp.model.data.House;
import ru.xmn.app1.mvp.model.data.Person;
import ru.xmn.app1.mvp.model.retrofit.ThronesApi;
import ru.xmn.app1.mvp.model.util.HouseNames;
import ru.xmn.app1.mvp.model.util.RealmString;
import ru.xmn.app1.mvp.model.util.RealmStringListTypeAdapter;
import rx.Observable;

/**
 * Created by xmn on 13.10.2016.
 */

public class SplashModel {
    private static final String TAG = "SplashModel";
    public SplashModel() {
        mRealmProvider = new RealmProvider();
    }

    //region retrofit
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(ThronesApi.URI)
            .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                    .registerTypeAdapter(new TypeToken<RealmList<RealmString>>() {
                            }.getType(),
                            RealmStringListTypeAdapter.INSTANCE).create()
            ))
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .build();

    ThronesApi mThronesApi = retrofit.create(ThronesApi.class);

    public ThronesApi getThronesApi() {
        return mThronesApi;
    }

    //endregion

    //region realm
    RealmProvider mRealmProvider;
    private List<House> mHouses = new ArrayList<>();

    ////@RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    public Observable<Boolean> getHousesObs() {
        return getThronesApi().getHouse(HouseNames.STARK.getId())
                .mergeWith(getThronesApi().getHouse(HouseNames.LANNISTER.getId()))
                .mergeWith(getThronesApi().getHouse(HouseNames.TARGARYEN.getId()))
                .doOnNext(this::houseToList)
                .toList()
                .flatMap(Observable::from)

                .flatMap(house -> Observable.from(house.getSwornMembers()))
                .distinct()
                .flatMap(s -> Observable.just(s.value.replaceAll("\\D+", "")))
                .map(Integer::valueOf)
                .flatMap(id -> getThronesApi().getPerson(id))
                .doOnNext(this::personToHouse)
                .toList()
                .flatMap(Observable::from)
                .flatMap(person -> Observable.merge(
                        Observable.just(person.getMother()),
                        Observable.just(person.getFather())
                ))
                .map(s -> s.replaceAll("\\D+", ""))
                .filter(s -> s.length() > 0)
                .map(Integer::valueOf)
                .flatMap(id -> getThronesApi().getPerson(id))
                .doOnNext(mRealmProvider::toRealm)
                .flatMap(person -> Observable.from(mHouses))
                .doOnNext(mRealmProvider::toRealm)
                .flatMap(houses -> Observable.just(true))
                .doOnError(Throwable::printStackTrace);
    }

    ////@RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    Observable<Boolean> housesToRealmWithDelay() {
        return Observable.zip(
                isDataLoaded() ? Observable.just(true) : getHousesObs(),
                Observable.timer(3, TimeUnit.SECONDS),
                (aBoolean, aLong) -> aBoolean);
    }

    private void houseToList(House house) {
        mHouses.add(house);
    }

    private void personToHouse(Person person) {
        for (int i = 0; i < mHouses.size(); i++) {
            if (person.getAllegiances().contains(new RealmString(mHouses.get(i).getUrl()))) {
                mHouses.get(i).addPerson(person);
            }
        }
    }

    boolean isDataLoaded() {
        boolean b = mRealmProvider.getHousesCount() > 0;
        //Log.d(TAG, "isDataLoaded() returned: " + b);
        return b;
    }
    //endregion
}
