package ru.xmn.app1.screens.house;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ru.xmn.app1.R;
import ru.xmn.app1.mvp.model.data.House;
import ru.xmn.app1.screens.house.recyclerview_fragment.HouseFragment;

/**
 * Created by xmn on 13.10.2016.
 */

public class HouseTabFragment extends Fragment implements HouseTabView {
    public TabLayout tabLayout;
    public ViewPager viewPager;
    public static int sItems = 3;
    Fragment mFragmentOne;
    Fragment mFragmentTwo;
    Fragment mFragmentThree;
    private List<House> mHouses;
    private HouseTabPresenter mPresenter = HouseTabPresenterImpl.getInstance();
    private static final String TAG = "HouseTabFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /**
         *Inflate tab_layout and setup Views.
         */
        View x = inflater.inflate(R.layout.fragment_house_tabs, null);
        mPresenter.takeView(this);

        tabLayout = (TabLayout) x.findViewById(R.id.tabs);
        viewPager = (ViewPager) x.findViewById(R.id.viewpager);
        return x;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated() called with: view = [" + view + "], savedInstanceState = [" + savedInstanceState + "]");
        mPresenter.initView();
    }

    public void setTab(int index) {
        viewPager.setCurrentItem(index);
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showError(Throwable error) {

    }

    @Override
    public void showLoad() {

    }

    @Override
    public void hideLoad() {

    }

    @Override
    public HouseTabPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void onHousesLoaded(List<House> houses) {
        Log.d(TAG, "onHousesLoaded() called with: houses = [" + houses + "]");
        mHouses = houses;
        mFragmentOne = getFragmentHouse(mHouses.get(0).getUrl());
        mFragmentTwo = getFragmentHouse(mHouses.get(1).getUrl());
        mFragmentThree = getFragmentHouse(mHouses.get(2).getUrl());

        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));
        tabLayout.post(() -> tabLayout.setupWithViewPager(viewPager));
    }

    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position .
         */

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return mFragmentOne;
                case 1:
                    return mFragmentTwo;
                case 2:
                    return mFragmentThree;
            }
            return null;
        }

        @Override
        public int getCount() {

            return sItems;

        }

        /**
         * This method returns the title of the tab according to the position.
         */

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return mHouses.get(position).getFormatedName();
                case 1:
                    return mHouses.get(position).getFormatedName();
                case 2:
                    return mHouses.get(position).getFormatedName();
            }
            return null;
        }
    }

    private Fragment getFragmentHouse(String houseStr) {
        Bundle args = new Bundle();
        args.putString("HOUSE", houseStr);
        HouseFragment newFragment = new HouseFragment();
        newFragment.setArguments(args);
        return newFragment;
    }
}
