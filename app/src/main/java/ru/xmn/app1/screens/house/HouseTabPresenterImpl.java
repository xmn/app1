package ru.xmn.app1.screens.house;

import android.support.annotation.Nullable;
import android.util.Log;

import io.realm.Realm;
import ru.xmn.app1.mvp.model.RealmProvider;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by xmn on 25.10.2016.
 */

public class HouseTabPresenterImpl implements HouseTabPresenter {
    private HouseTabView mView;
    private RealmProvider mRealmProvider;
    private static HouseTabPresenterImpl sInstance = new HouseTabPresenterImpl();
    private static final String TAG = "HouseTabPresenterImpl";
    public static HouseTabPresenter getInstance() {
        return sInstance;
    }

    public HouseTabPresenterImpl() {
        mRealmProvider = new RealmProvider();
    }

    //region custom presenter methods
    @Override
    public void takeView(HouseTabView view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public void initView() {
        mView.showLoad();
        Realm realm = Realm.getDefaultInstance();
        Log.d(TAG, "initView() called " + Thread.currentThread().getName());
        mRealmProvider.getHousesCopiesAsync(realm)
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnCompleted(realm::close)
                .subscribe(mView::onHousesLoaded);
    }

    @Nullable
    @Override
    public HouseTabView getView() {
        return mView;
    }
    //endregion


}
