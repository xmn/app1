package ru.xmn.app1.screens.house.recyclerview_fragment;

import ru.xmn.app1.mvp.presenter.BasePresenter;

/**
 * Created by xmn on 25.10.2016.
 */

public interface HousePresenter extends BasePresenter<HouseView> {
    void loadPersonList(String house);
}
