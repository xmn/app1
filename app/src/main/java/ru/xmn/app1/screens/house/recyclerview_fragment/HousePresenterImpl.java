package ru.xmn.app1.screens.house.recyclerview_fragment;

import android.support.annotation.Nullable;

import io.realm.Realm;
import ru.xmn.app1.mvp.model.RealmProvider;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by xmn on 25.10.2016.
 */

public class HousePresenterImpl implements HousePresenter {
//    private static HousePresenter sInstance = new HousePresenterImpl();
    private HouseView mView;
    private RealmProvider mRealmProvider;
    public HousePresenterImpl(HouseView view) {
        mRealmProvider = new RealmProvider();
        mView = view;
    }

//    public static HousePresenter getInstance() {
//        return sInstance;
//    }

    @Override
    public void takeView(HouseView view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public void initView() {

    }

    @Nullable
    @Override
    public HouseView getView() {
        return mView;
    }

    @Override
    public void loadPersonList(String house) {
        Realm realm = Realm.getDefaultInstance();
        mRealmProvider.getPersonCopiesAsync(realm, house)
                .toList()
                .cache()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnCompleted(realm::close)
                .subscribe(persons -> mView.onPersonsLoaded(persons));
    }
}
