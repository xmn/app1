package ru.xmn.app1.screens.splash;

import android.support.annotation.Nullable;
import android.util.Log;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.fernandocejas.frodo.annotation.RxLogSubscriber;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import ru.xmn.app1.mvp.model.data.House;
import ru.xmn.app1.mvp.model.util.HouseNames;
import ru.xmn.app1.mvp.model.data.Person;
import ru.xmn.app1.mvp.model.util.RealmString;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by xmn on 13.10.2016.
 */

class SplashPresenterImpl implements SplashPresenter {

    private SplashView mView;
    private SplashModel mModel;
    private static SplashPresenterImpl Instance = new SplashPresenterImpl();

    private SplashPresenterImpl() {
        mModel = new SplashModel();
    }

    static SplashPresenterImpl getInstance() {
        return Instance;
    }

    //region Common presenter methods
    @Override
    public void takeView(SplashView view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView=null;
    }

    @Override
    public void initView() {
        mModel.housesToRealmWithDelay().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new toMainActivity());
    }

    @Nullable
    @Override
    public SplashView getView() {
        return mView;
    }
    //endregion

    //@RxLogSubscriber
    private class toMainActivity extends Subscriber {
        @Override
        public void onCompleted() {
        }

        @Override
        public void onError(Throwable e) {
            mView.showError(e);
        }

        @Override
        public void onNext(Object o) {
            mView.loadCompleted();
        }
    }


}
