package ru.xmn.app1.screens.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import ru.xmn.app1.BuildConfig;
import ru.xmn.app1.R;
import ru.xmn.app1.mvp.model.data.House;
import ru.xmn.app1.mvp.model.data.Person;
import ru.xmn.app1.mvp.presenter.BasePresenter;
import ru.xmn.app1.screens.house.HouseActivity;

/**
 * Created by xmn on 13.10.2016.
 */

public class SplashFragment extends Fragment implements SplashView {
    SplashPresenter mPresenter = SplashPresenterImpl.getInstance();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_splash, container, false);
        mPresenter.takeView(this);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.initView();
    }

    public void loadCompleted() {
//        showHouses();
        Intent i = new Intent(getActivity(), HouseActivity.class);
        getActivity().startActivity(i);
    }

    private void showHouses() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<House> houses = realm.where(House.class).findAll();
        RealmList<House> results = new RealmList<>();
        results.addAll(houses.subList(0, houses.size()));
        for (House h :
                results) {
            //Log.d(this.getClass().getSimpleName(), h + " " + h.getPersons().size() + " " + h.getSwornMembers().size());
            for (Person p :
                    h.getPersons()) {
            //Log.d(this.getClass().getSimpleName(), p.toString());
            }
        }
        realm.close();
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showError(Throwable error) {
        if (BuildConfig.DEBUG) {
            showMessage(error.getMessage());
            error.printStackTrace();
        } else {
            showMessage("error");
            // TODO: 20.10.2016 sent to crashlytics
        }
    }

    @Override
    public void showLoad() {

    }

    @Override
    public void hideLoad() {

    }

    @Override
    public SplashPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }
}
