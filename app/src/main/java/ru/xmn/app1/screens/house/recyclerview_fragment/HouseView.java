package ru.xmn.app1.screens.house.recyclerview_fragment;

import java.util.List;

import ru.xmn.app1.mvp.model.data.Person;
import ru.xmn.app1.mvp.view.BaseView;

/**
 * Created by xmn on 25.10.2016.
 */

public interface HouseView extends BaseView<HousePresenter> {
    void onPersonsLoaded(List<Person> persons);
}
