package ru.xmn.app1.mvp.model.data;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import ru.xmn.app1.mvp.model.util.RealmString;

/**
 * Created by xmn on 13.10.2016.
 */

public class Person extends RealmObject {
    public Person() {
    }

    @PrimaryKey
    private String url;
    private String name;
    private String gender;
    private String culture;
    private String born;
    private String died;
    private RealmList<RealmString> titles = new RealmList<>();
    private RealmList<RealmString> aliases = new RealmList<>();
    private String father;
    private String mother;
    private String spouse;
    private RealmList<RealmString> allegiances = new RealmList<>();
    private RealmList<RealmString> books = new RealmList<>();
    private RealmList<RealmString> povBooks = new RealmList<>();
    private RealmList<RealmString> tvSeries = new RealmList<>();
    private RealmList<RealmString> playedBy = new RealmList<>();

    /**
     * @return The url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender The gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return The culture
     */
    public String getCulture() {
        return culture;
    }

    /**
     * @param culture The culture
     */
    public void setCulture(String culture) {
        this.culture = culture;
    }

    /**
     * @return The born
     */
    public String getBorn() {
        return born;
    }

    /**
     * @param born The born
     */
    public void setBorn(String born) {
        this.born = born;
    }

    /**
     * @return The died
     */
    public String getDied() {
        return died;
    }

    /**
     * @param died The died
     */
    public void setDied(String died) {
        this.died = died;
    }

    /**
     * @return The titles
     */
    public RealmList<RealmString> getTitles() {
        return titles;
    }

    /**
     * @param titles The titles
     */
    public void setTitles(RealmList<RealmString> titles) {
        this.titles = titles;
    }

    /**
     * @return The aliases
     */
    public RealmList<RealmString> getAliases() {
        return aliases;
    }

    /**
     * @param aliases The aliases
     */
    public void setAliases(RealmList<RealmString> aliases) {
        this.aliases = aliases;
    }

    /**
     * @return The father
     */
    public String getFather() {
        return father;
    }

    /**
     * @param father The father
     */
    public void setFather(String father) {
        this.father = father;
    }

    /**
     * @return The mother
     */
    public String getMother() {
        return mother;
    }

    /**
     * @param mother The mother
     */
    public void setMother(String mother) {
        this.mother = mother;
    }

    /**
     * @return The spouse
     */
    public String getSpouse() {
        return spouse;
    }

    /**
     * @param spouse The spouse
     */
    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    /**
     * @return The allegiances
     */
    public RealmList<RealmString> getAllegiances() {
        return allegiances;
    }

    /**
     * @param allegiances The allegiances
     */
    public void setAllegiances(RealmList<RealmString> allegiances) {
        this.allegiances = allegiances;
    }

    /**
     * @return The books
     */
    public RealmList<RealmString> getBooks() {
        return books;
    }

    /**
     * @param books The books
     */
    public void setBooks(RealmList<RealmString> books) {
        this.books = books;
    }

    /**
     * @return The povBooks
     */
    public RealmList<RealmString> getPovBooks() {
        return povBooks;
    }

    /**
     * @param povBooks The povBooks
     */
    public void setPovBooks(RealmList<RealmString> povBooks) {
        this.povBooks = povBooks;
    }

    /**
     * @return The tvSeries
     */
    public RealmList<RealmString> getTvSeries() {
        return tvSeries;
    }

    /**
     * @param tvSeries The tvSeries
     */
    public void setTvSeries(RealmList<RealmString> tvSeries) {
        this.tvSeries = tvSeries;
    }

    /**
     * @return The playedBy
     */
    public RealmList<RealmString> getPlayedBy() {
        return playedBy;
    }

    /**
     * @param playedBy The playedBy
     */
    public void setPlayedBy(RealmList<RealmString> playedBy) {
        this.playedBy = playedBy;
    }

    @Override
    public String toString() {
        return "Person{" +
                "url='" + url + '\'' +
                ", name='" + name + '\'' +
                ", father='" + father + '\'' +
                ", mother='" + mother + '\'' +
                '}';
    }
}