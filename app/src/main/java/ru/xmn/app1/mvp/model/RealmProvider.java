package ru.xmn.app1.mvp.model;

import android.util.Log;

import com.fernandocejas.frodo.annotation.RxLogObservable;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.RealmStringRealmProxy;
import ru.xmn.app1.mvp.model.data.House;
import ru.xmn.app1.mvp.model.data.Person;
import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by xmn on 25.10.2016.
 */

public class RealmProvider {
    private static final String TAG = "RealmProvider";
    public <T extends RealmObject> void toRealm(T t) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(t);
        realm.commitTransaction();
        realm.close();
    }

//    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    public Observable<House> getHousesCopiesAsync(Realm realm) {
        return realm.where(House.class)
                .findAllAsync()
                .asObservable()
                .filter(RealmResults::isLoaded)
                .first()
                .map(realm::copyFromRealm)
                .observeOn(Schedulers.io())
                .flatMap(Observable::from);
    }

//    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    public Observable<Person> getPersonCopiesAsync(Realm realm, String house) {
        Log.d(TAG, "getPersonCopiesAsync() called with: realm = [" + realm + "], house = [" + house + "]");
        return realm.where(House.class)
                .equalTo("url", house)
                .findAllAsync()
                .asObservable()
                .filter(RealmResults::isLoaded)
                .first()
                .map(realm::copyFromRealm)
                .flatMap(Observable::from)
                .flatMap(house1 -> Observable.from(house1.getPersons()));
    }

    //@RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    public Observable<House> getHouses(Realm realm) {
        return realm.where(House.class)
                .findAllAsync()
                .asObservable()
                .filter(RealmResults::isLoaded)
                .first()
                .flatMap(Observable::from);
    }

    public long getHousesCount() {
        Realm realm = Realm.getDefaultInstance();
        long count = realm.where(House.class).count();
        realm.close();
        //Log.d(TAG, "getHousesCount() returned: " + count);
        return count;
    }

    public Observable<Person> getPerson(Realm realm, String personUrl) {
        return realm.where(Person.class)
                .equalTo("url", personUrl)
                .findFirstAsync()
                .asObservable()
                .filter(realmObject -> realmObject.isLoaded())
                .first()
                .map(realmObject -> (Person) realmObject);
    }

    public Observable<House> getHouse(Realm realm, String houseUrl) {
        return realm.where(House.class)
                .equalTo("url", houseUrl)
                .findFirstAsync()
                .asObservable()
                .filter(realmObject -> realmObject.isLoaded())
                .first()
                .map(realmObject -> (House) realmObject);
    }
}
