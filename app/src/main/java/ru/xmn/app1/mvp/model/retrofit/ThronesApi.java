package ru.xmn.app1.mvp.model.retrofit;

import retrofit2.http.GET;
import retrofit2.http.Path;
import ru.xmn.app1.mvp.model.data.House;
import ru.xmn.app1.mvp.model.data.Person;
import rx.Observable;

/**
 * Created by xmn on 13.10.2016.
 */

public interface ThronesApi {
    String URI = "http://anapioficeandfire.com/api/";

    @GET("houses/{id}")
    Observable<House> getHouse(@Path("id") int id);

    @GET("characters/{id}")
    Observable<Person> getPerson(@Path("id") int id);
}