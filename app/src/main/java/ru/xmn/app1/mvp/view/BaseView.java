package ru.xmn.app1.mvp.view;

import ru.xmn.app1.mvp.presenter.BasePresenter;

/**
 * Created by xmn on 24.10.2016.
 */

public interface BaseView <T extends BasePresenter> {
    void showMessage(String message);

    void showError(Throwable error);

    void showLoad();

    void hideLoad();

    T getPresenter();
}
