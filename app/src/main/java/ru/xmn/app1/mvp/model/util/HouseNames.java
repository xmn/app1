package ru.xmn.app1.mvp.model.util;

import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;

/**
 * Created by xmn on 16.10.2016.
 */

public enum HouseNames {
    STARK(362), LANNISTER(229), TARGARYEN(378);
    private int id;

    HouseNames(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
