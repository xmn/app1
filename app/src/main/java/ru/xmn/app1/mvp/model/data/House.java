package ru.xmn.app1.mvp.model.data;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import ru.xmn.app1.mvp.model.util.RealmString;

/**
 * Created by xmn on 13.10.2016.
 */

public class House extends RealmObject {
    public House() {
    }

    @PrimaryKey
    private String url;
    private String name;
    public String getFormatedName(){
        return name.split(" ")[1].toUpperCase();
    };
    private String region;
    private String coatOfArms;
    private String words;
    private RealmList<RealmString> titles = new RealmList<RealmString>();
    private RealmList<RealmString> seats = new RealmList<RealmString>();
    private String currentLord;
    private String heir;
    private String overlord;
    private String founded;
    private String founder;
    private String diedOut;
    private RealmList<RealmString> ancestralWeapons = new RealmList<RealmString>();
    private RealmList<RealmString> cadetBranches = new RealmList<RealmString>();
    private RealmList<RealmString> swornMembers = new RealmList<RealmString>();
    private RealmList<Person> persons = new RealmList<>();

    public RealmList<Person> getPersons() {
        return persons;
    }

    public void addPerson(Person person) {
        persons.add(person);
    }

    /**
     * @return The url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The region
     */
    public String getRegion() {
        return region;
    }

    /**
     * @param region The region
     */
    public void setRegion(String region) {
        this.region = region;
    }

    /**
     * @return The coatOfArms
     */
    public String getCoatOfArms() {
        return coatOfArms;
    }

    /**
     * @param coatOfArms The coatOfArms
     */
    public void setCoatOfArms(String coatOfArms) {
        this.coatOfArms = coatOfArms;
    }

    /**
     * @return The words
     */
    public String getWords() {
        return words;
    }

    /**
     * @param words The words
     */
    public void setWords(String words) {
        this.words = words;
    }

    /**
     * @return The titles
     */
    public RealmList<RealmString> getTitles() {
        return titles;
    }

    /**
     * @param titles The titles
     */
    public void setTitles(RealmList<RealmString> titles) {
        this.titles = titles;
    }

    /**
     * @return The seats
     */
    public RealmList<RealmString> getSeats() {
        return seats;
    }

    /**
     * @param seats The seats
     */
    public void setSeats(RealmList<RealmString> seats) {
        this.seats = seats;
    }

    /**
     * @return The currentLord
     */
    public String getCurrentLord() {
        return currentLord;
    }

    /**
     * @param currentLord The currentLord
     */
    public void setCurrentLord(String currentLord) {
        this.currentLord = currentLord;
    }

    /**
     * @return The heir
     */
    public String getHeir() {
        return heir;
    }

    /**
     * @param heir The heir
     */
    public void setHeir(String heir) {
        this.heir = heir;
    }

    /**
     * @return The overlord
     */
    public String getOverlord() {
        return overlord;
    }

    /**
     * @param overlord The overlord
     */
    public void setOverlord(String overlord) {
        this.overlord = overlord;
    }

    /**
     * @return The founded
     */
    public String getFounded() {
        return founded;
    }

    /**
     * @param founded The founded
     */
    public void setFounded(String founded) {
        this.founded = founded;
    }

    /**
     * @return The founder
     */
    public String getFounder() {
        return founder;
    }

    /**
     * @param founder The founder
     */
    public void setFounder(String founder) {
        this.founder = founder;
    }

    /**
     * @return The diedOut
     */
    public String getDiedOut() {
        return diedOut;
    }

    /**
     * @param diedOut The diedOut
     */
    public void setDiedOut(String diedOut) {
        this.diedOut = diedOut;
    }

    /**
     * @return The ancestralWeapons
     */
    public RealmList<RealmString> getAncestralWeapons() {
        return ancestralWeapons;
    }

    /**
     * @param ancestralWeapons The ancestralWeapons
     */
    public void setAncestralWeapons(RealmList<RealmString> ancestralWeapons) {
        this.ancestralWeapons = ancestralWeapons;
    }

    /**
     * @return The cadetBranches
     */
    public RealmList<RealmString> getCadetBranches() {
        return cadetBranches;
    }

    /**
     * @param cadetBranches The cadetBranches
     */
    public void setCadetBranches(RealmList<RealmString> cadetBranches) {
        this.cadetBranches = cadetBranches;
    }

    /**
     * @return The swornMembers
     */
    public RealmList<RealmString> getSwornMembers() {
        return swornMembers;
    }

    /**
     * @param swornMembers The swornMembers
     */
    public void setSwornMembers(RealmList<RealmString> swornMembers) {
        this.swornMembers = swornMembers;
    }

    @Override
    public String toString() {
        return "House{" +
                "name='" + name + '\'' +
                ", persons=" + persons +
                '}';
    }
}