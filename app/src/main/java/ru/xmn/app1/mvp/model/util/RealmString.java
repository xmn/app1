package ru.xmn.app1.mvp.model.util;

import io.realm.RealmObject;

/**
 * Created by xmn on 14.10.2016.
 */

public class RealmString extends RealmObject {
    public String value;

    public RealmString(String value) {
        this.value = value;
    }
    public RealmString() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RealmString that = (RealmString) o;

        return value != null ? value.equals(that.value) : that.value == null;

    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }
}