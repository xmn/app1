package ru.xmn.app1.mvp.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import ru.xmn.app1.R;


public abstract class SingleFragmentActivity extends AppCompatActivity {

    private Fragment mFragment;
    private FragmentManager mManager;

    protected abstract Fragment createFragment();

    protected int getLayoutResId() {
        return R.layout.activity_single_fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
        mManager = getSupportFragmentManager();
        mFragment = mManager.findFragmentById(R.id.fragmentContainer);
        if (mFragment == null) {
            mFragment = createFragment();
            mFragment.setArguments(getIntent().getExtras());
            mManager.beginTransaction()
                    .add(R.id.fragmentContainer, mFragment)
                    .commit();}
        }


}
