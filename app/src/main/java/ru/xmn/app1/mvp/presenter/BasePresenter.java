package ru.xmn.app1.mvp.presenter;

import android.support.annotation.Nullable;

import ru.xmn.app1.mvp.view.BaseView;

/**
 * Created by xmn on 24.10.2016.
 */

public interface BasePresenter <T extends BaseView>{

    void takeView(T view);

    void dropView();

    void initView();

    @Nullable
    T getView();
}
