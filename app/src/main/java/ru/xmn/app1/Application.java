package ru.xmn.app1;

import android.widget.Toast;

import io.realm.Realm;
import io.realm.RealmConfiguration;
//import ru.xmn.concert.view.MainActivity;

public class Application extends android.app.Application {

    private static Application instance;

    public static Application get() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        realmInit();
    }

    private void realmInit() {
        Realm.setDefaultConfiguration(
                new RealmConfiguration.Builder(Application.get().getApplicationContext())
                        .deleteRealmIfMigrationNeeded()
                        .name("myOtherRealm.realm")
                        .build()
        );
    }

}